package com.example;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MainTest {

  @Test
  public void message() {
    assertThat(Main.message(), is("Hello world!"));
  }
}
